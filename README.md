# Pyinstall fabric proof of concept

## Files

### cli.py

Override default *fab* entry point and associate custom set of modules.

### fabfile.py

Load modules

### angryip.py

Example module

## Test entry point 

	pip install -e .
	pro -V
	pro angryip.init
