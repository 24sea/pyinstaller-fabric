"""Invoke tasks for managing the AngryIP application"""
from invoke import task

# Registry key
reg_key = r"HKCU\Software\JavaSoft\Prefs\ipscan"


@task
def init(c):
    """Set some AngryIP settings in the registry

    +-----------------------+--------------------------------------------+
    | **key**               | **value**                                  |
    +-----------------------+--------------------------------------------+
    | ask/Scan/Confirmation | false                                      |
    +-----------------------+--------------------------------------------+
    | display/Method        | all                                        |
    +-----------------------+--------------------------------------------+
    | first/Run             | false                                      |
    +-----------------------+--------------------------------------------+
    | port/String           | 22,80,443,3389                             |
    +-----------------------+--------------------------------------------+
    | scan/Dead/Hosts       | true                                       |
    +-----------------------+--------------------------------------------+
    | selected/Fetchers     | ip, hostname, ping, ports, mac, mac.vendor |
    +-----------------------+--------------------------------------------+
    | show/Scan/Stats       | false                                      |
    +-----------------------+--------------------------------------------+
    | use/Requested/Ports   | true                                       |
    +-----------------------+--------------------------------------------+
    """
    reg_values = {
        "ask/Scan/Confirmation": "false",
        "display/Method": "/A/L/L",
        "first/Run": "false",
        "port/String": "22,80,443,3389",
        "scan/Dead/Hosts": "true",
        "selected/Fetchers": "fetcher.ip###fetcher.hostname###fetcher.ping###fetcher.ports###fetcher.mac###fetcher.mac.vendor",
        "show/Scan/Stats": "false",
        "use/Requested/Ports": "true",
    }

    print(reg_values)
