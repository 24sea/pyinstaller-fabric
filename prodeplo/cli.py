from fabric import Config
from fabric.executor import Executor
from fabric.main import Fab

from prodeplo import fabfile


def program():
    """The *pro* cli entry point"""
    pro = Fab(
        name="prodeplo",
        version="0.20.0-dev",
        executor_class=Executor,
        config_class=Config,
        namespace=fabfile.ns,
    )

    pro.run()
