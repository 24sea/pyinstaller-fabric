"""The fabfile is the default file being read by the command-line tool fab

Build a collection of tasks from other modules here
"""

import importlib

from invoke import Collection

modules = [
    "angryip",
]

ns = Collection()
for module in modules:
    module = importlib.import_module(f"prodeplo.{module}")
    ns.add_collection(Collection.from_module(module))
