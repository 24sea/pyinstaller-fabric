import setuptools

setuptools.setup(
    name="prodeplo",
    version="0.20.0-dev",
    url="https://bitbucket.org/24sea/prodeplo",
    author="Tim",
    author_email="",
    description="",
    long_description=open("README.md").read(),
    packages=setuptools.find_packages(),
    install_requires=["fabric"],
    entry_points={"console_scripts": ["pro=prodeplo.cli:program"]},
)
